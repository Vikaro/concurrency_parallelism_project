package cp.articlerep;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Mateusz on 29.11.2016.
 */
public class Stop {
    public static Lock globalLock = new ReentrantLock(true);

    public static Lock [] arrayLock;
    private static boolean [] FLAG;
    private static int TURN;
    public static int n; // number of threads
    public static void InitializeStop(int dictSize){
        arrayLock = new ReentrantLock[dictSize];
        FLAG = new boolean[dictSize];
        TURN = 0;
        for(int i = 0 ; i <dictSize; i++){
            arrayLock[i] = new ReentrantLock();
        }
    }
    public static void acquire_mutex(int i){
        FLAG[i] = true;
        while ( TURN != i && FLAG[TURN]){
            System.out.println("waiting");
        }
        arrayLock[i].lock();
        System.out.println("acquire mutex: " + i);
    }
    public static void release_mutex(int i){
        FLAG[i] = false;
        if(!FLAG[TURN]){
            TURN = (TURN % n) +1;
        }
        arrayLock[i].unlock();
        System.out.println("release mutex: " + i);

    }

}
